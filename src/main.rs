use dirs::home_dir;
use regex::Regex;
use std::fmt::Write as WriteToString;
use std::fs::File;
use std::io::Write;
use std::{env, fs, io};

fn frames_to_timecode(total_frames: i32, fps: i32) -> String {
    // let hours = total_frames / (3600 * fps);
    let minutes = total_frames / (60 * fps) % 60;
    let mut seconds = total_frames / fps % 60;
    let frames = total_frames % fps;

    if frames > 30 {
        seconds = seconds + 1;
    }

    let mut _timecode: String = String::new();
    if seconds < 10 {
        _timecode = format!("{minutes}:0{seconds}:{frames}");
    } else {
        _timecode = format!("{minutes}:{seconds}:{frames}");
    }
    _timecode
}

fn main() -> io::Result<()> {
    let args: Vec<_> = env::args().collect();
    let input_file = args[1].to_string();
    let file = fs::read_to_string(input_file).expect("Failed to read file");

    // regexes
    let pos = Regex::new(
        r"(?x)
        \s*\b(?P<pos>pos)\b.*$           # frame
    ",
    )
    .unwrap();
    // TODO: kdenlive updated something n now we need to filter titles from
    // color information
    let title = Regex::new(
        r#"(?x)
        \s*(["]comment)\b.*$             # title
    "#,
    )
    .unwrap();
    let fps = Regex::new(
        r"(?x)
        \s*\b(?P<fps>fps)\b.*$           # fps
    ",
    )
    .unwrap();
    let project_name = Regex::new(
        r"(?x)
        \s*\b(?P<consumer>consumer)\b.*$ # filename
    ",
    )
    .unwrap();

    // collect titles n timecodes for processing
    let mut v_titles: Vec<String> = Vec::new();
    let mut v_timecodes: Vec<String> = Vec::new();
    let mut fps_to_int: i32 = 0;
    let mut filename: String = String::new();

    for line in file.lines() {
        if let Some(_c) = fps.captures(line) {
            let fps: Vec<&str> = line.split('"').collect();
            fps_to_int = fps[1].parse::<i32>().unwrap();
        }
        if let Some(_c) = pos.captures(line) {
            let frame_pos: Vec<&str> = line.split(":").collect();
            let frame_pos = frame_pos[1].replace(",", "");
            let frame_pos = frame_pos.trim();
            let frame_pos = frame_pos.parse::<i32>().unwrap();
            let timecode = frames_to_timecode(frame_pos, fps_to_int);

            v_timecodes.push(timecode);
        }
        if let Some(_c) = title.captures(line) {
            let titles: Vec<&str> = line.split(":").collect();
            let titles = titles[1].replace(",", "").replace('"', "");
            let titles = titles.trim().to_string();

            v_titles.push(titles);
        }
        if let Some(_c) = project_name.captures(line) {
            let project_name: Vec<&str> = line.split("target=\"").collect();
            let project_name = project_name[1].to_string();
            let project_name: Vec<&str> = project_name.split("\"").collect();
            let project_name = project_name[0].to_string();
            let project_name: Vec<&str> = project_name.split("/").collect();
            let project_name = project_name[project_name.len() - 1].to_string();
            let filename_vec: Vec<&str> = project_name.split(".").collect();

            filename = filename_vec[0].to_string();
        }
    }

    let mut chapters = "0:00 - Intro\n".to_string();
    let mut index = 0;
    for _i in v_titles.iter() {
        // TODO: currently if the video is over 10min, formatting breaks
        // due to write!(:.4) -> needs to be :.5 for timecodes
        // that go over the 10min mark
        write!(chapters, "{:.5} - ", v_timecodes[index]).expect("Failed to write timecode");
        writeln!(chapters, "{}", v_titles[index]).unwrap();
        index += 1;
    }

    // create txt file to desktop
    let mut file_path = home_dir().expect("no home dir");
    file_path.push(format!("{}.txt", filename));

    let mut file = File::create(file_path).expect("Failed to create file");
    file.write(chapters.as_bytes())
        .expect("Failed to write to file");
    println!(
        "==> Chapters written to file!
  -> File saved to HOME as {}.txt",
        filename
    );

    Ok(())
}
