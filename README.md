# COPY KDENLIVE CHAPTERS TO FILE

## Description
Copy chapters from KDENLIVE render scripts to a text file.

At the time of writing KDENLIVE doesn't yet allow you to directly export chapters
to an external file. This simple program takes the mlt file generated
when creating a render script, iterates through it and stores the fps, chapter names,
and timecodes and saves them in a txt file as `$HOME/<your_user>/<name_of_project>.txt`

In KDENLIVE, simply set the markers to where you want them and name them.

In the actual mlt file, the timecode is represented as frames, so to start things
off, the program collects all the markers and their respective names.
These are then converted back to timecode in the following format (hh:mm:ss:ff).

To accurately convert the frames, we need to know the fps of the video. 
This is also collected from the mlt file and fed to the function handling the 
conversion.

This was done out of personal need. It's not pretty nor optimized, but it works.
And hey, its written in Rust, so it's blessed by everybody's favorite rustacean, Ferris.

## Example
In a terminal, run `copy_chapters_to_file <path_to_render_script>.mlt`

Example output file contents:
>0:00 - Intro
>
>0:16 - Ferris Dances
>
>2:52 - Outro

## Whats needed for 1.0.0 Release
>Support entire directories
>
>Improved error handling
>
>Tests (a lot of them)
